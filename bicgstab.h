/*
 * BiCGStab iterative linear system solver
 * Copyright (C) 2016 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEUKOLSKY_DATA_BICGSTAB_H
#define TEUKOLSKY_DATA_BICGSTAB_H

#include "config.h"

typedef struct BiCGStabContext BiCGStabContext;

/**
 * Allocate and initialize the solver for an NxN system.
 */
int tdi_bicgstab_context_alloc(BiCGStabContext **ctx, int N, int maxiter);

/**
 * Free the solver and all its internal state.
 */
void tdi_bicgstab_context_free(BiCGStabContext **ctx);

/**
 * Initialise the solver with the given preconditioner matrix. This function
 * may be called any number of times on a given solver context.
 */
int tdi_bicgstab_init(BiCGStabContext *ctx, const double *k, const double *x0);

/**
 * Solve the linear system
 * mat · x = rhs
 * The result is written into x.
 */
int tdi_bicgstab_solve(BiCGStabContext *ctx, const double *mat, const double *rhs, double *x);

#endif /* TEUKOLSKY_DATA_BICGSTAB_H */
