TARGET = libteukolskydata.so

CFLAGS = -std=c99 -D_XOPEN_SOURCE=700 -fPIC -g -I.
TARGET_LDFLAGS = -Wl,--version-script=libteukolskydata.v -shared -lm -llapacke -lthreadpool
TEST_LIBS = -lm -llapacke -lcblas -lpthread
CC = cc

OBJS = basis.o               \
       bicgstab.o            \
       eval.o                \
       init.o                \
       log.o                 \
       nlsolve.o             \
       pssolve.o             \
       td_constraints.o      \

TESTPROGS = nlsolve          \
            pssolve

TESTPROGS := $(addprefix tests/,$(TESTPROGS))

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -o $@ $(OBJS) ${TARGET_LDFLAGS}

%.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

%.o: %.asm
	nasm -f elf64 -M $< > $(@:.o=.d)
	nasm -f elf64 -o $@ $<

clean:
	-rm -f *.o *.d *.pyc $(TARGET)

tests/%.o: tests/%.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

tests/%: tests/%.o $(OBJS)
	$(CC) -o $@ $(@:=.o) $(OBJS) $(TEST_LIBS)

test: $(TARGET) $(TESTPROGS)
	LD_LIBRARY_PATH=. PYTHONPATH=. ./tests/convergence.py

-include $(OBJS:.o=.d)

.PHONY: clean test
