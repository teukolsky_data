/*
 * Copyright 2017 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEUKOLSKY_DATA_CONSTRAINTS_H
#define TEUKOLSKY_DATA_CONSTRAINTS_H

#include "pssolve.h"
#include "teukolsky_data.h"

enum TDConstraintEq {
    TD_CONSTRAINT_EQ_HAM = 0,
    TD_CONSTRAINT_EQ_MOM_0,
    TD_CONSTRAINT_EQ_MOM_1,
    TD_CONSTRAINT_EQ_NB,
};

enum TDConstraintVar {
    TD_CONSTRAINT_VAR_CONFFAC = 0,
    TD_CONSTRAINT_VAR_CURV_RR,
    TD_CONSTRAINT_VAR_CURV_PHIPHI,
    TD_CONSTRAINT_VAR_NB,
};


typedef struct TDConstraintEvalPriv TDConstraintEvalPriv;

typedef struct TDConstraintEvalContext {
    /**
     * Private data, not to be touched by the caller.
     */
    TDConstraintEvalPriv *priv;

    /**
     * The logging context.
     * Set by the caller before tdi_constraint_eval_init().
     */
    TDLogger logger;

    unsigned int nb_coords[2];
    const double *coords[2];

    enum TDFamily family;

    double amplitude;
    double a_converge;
    double a_diverge;
} TDConstraintEvalContext;

int tdi_constraint_eval_alloc(TDConstraintEvalContext **ctx, enum TDFamily family);
int tdi_constraint_eval_init (TDConstraintEvalContext  *ctx);
void tdi_constraint_eval_free(TDConstraintEvalContext **ctx);

int tdi_constraint_eval(TDConstraintEvalContext *ctx, enum TDConstraintEq eq,
                        const double * const vars[TD_CONSTRAINT_VAR_NB][PSSOLVE_DIFF_ORDER_NB],
                        double *out);

double tdi_constraint_eval_k_rtheta(TDConstraintEvalContext *ctx, double r, double theta);
double tdi_constraint_eval_dk_rtheta_r(TDConstraintEvalContext *ctx, double r, double theta);
double tdi_constraint_eval_dk_rtheta_t(TDConstraintEvalContext *ctx, double r, double theta);

#endif // TEUKOLSKY_DATA_CONSTRAINTS_H
