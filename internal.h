/*
 * Copyright 2017-2022 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEUKOLSKY_DATA_INTERNAL_H
#define TEUKOLSKY_DATA_INTERNAL_H

#include "basis.h"
#include "log.h"
#include "td_constraints.h"
#include "teukolsky_data.h"

#define NB_EQUATIONS 3

typedef struct TDPriv {
    unsigned int basis_order[NB_EQUATIONS][2];
    BasisSetContext *basis[NB_EQUATIONS][2];

    TPContext *tp;
    TDLogger logger;

    double *coeffs;
    double *coeffs_tmp;
    ptrdiff_t coeffs_stride;

    double *coeffs_lapse;
} TDPriv;

int tdi_ce_alloc(const TDContext *td, const unsigned int *nb_coords,
                 const double * const *coords,
                 double amplitude, TDConstraintEvalContext **pce);

#endif // TEUKOLSKY_DATA_INTERNAL_H
