/*
 * Basis sets for pseudospectral methods
 * Copyright 2017 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEUKOLSKY_DATA_BASIS_H
#define TEUKOLSKY_DATA_BASIS_H

enum BSEvalType {
    BS_EVAL_TYPE_VALUE,
    BS_EVAL_TYPE_DIFF1,
    BS_EVAL_TYPE_DIFF2,
};

enum BasisFamily {
    BASIS_FAMILY_TB_EVEN,
    BASIS_FAMILY_SB_EVEN,
    BASIS_FAMILY_SB_ODD,
    BASIS_FAMILY_COS,
    BASIS_FAMILY_COS_EVEN,
    BASIS_FAMILY_COS_4,
};

typedef struct BasisSetContext BasisSetContext;

int tdi_basis_init(BasisSetContext **ctx, enum BasisFamily family, double sf);
void tdi_basis_free(BasisSetContext **ctx);

double tdi_basis_eval(const BasisSetContext *ctx, enum BSEvalType type,
                      double coord, unsigned int order);
double tdi_basis_colloc_point(const BasisSetContext *ctx, unsigned int order,
                              unsigned int idx);

#endif /* TEUKOLSKY_DATA_BASIS_H */
